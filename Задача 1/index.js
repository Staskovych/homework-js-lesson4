/*
Задача 1 
Напиши функцию map(fn, array), которая принимает на вход функцию и массив, 
и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.
*/ 

const arr = [1,2,3,4,5];

function callback (number) {
  return number * 5;
}

function map (array, funcCallback) {
  const newArr = [];

  for (let i = 0; i < array.length; i++) {
    const resultCallbackFunc = funcCallback(array[i])
    newArr.push(resultCallbackFunc);
  }
  return newArr;
}

console.log(map(arr, callback));













  










